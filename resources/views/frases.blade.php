@extends('base')

@section('title')
    Administración de Frases
@stop

@section('content')
    <section class="container-fluid">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Frases</h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-6">
                            <form method="post">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                <div class="form-group">
                                    <label for="frase" class="control-label">Ingrese una frase</label>
                                    <input id="frase" class="form-control" type="text" name="frase" required="required" maxlength="200"/>
                                </div>
                                <input type="submit" name="register_frase" class="btn btn-primary single-click" value="Registrar Frase Individual" />
                            </form>
                        </div>
                        <div class="col-md-6">
                            <form method="post" enctype="multipart/form-data">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                <div class="form-group">
                                    <label for="frases_file" class="control-label">Cargue las frases desde un Archivo</label>
                                    <input id="frases_file" class="form-control" type="file" name="frases_file" required="required"/>
                                </div>
                                <input type="submit" name="register_frase_multiple" class="btn btn-primary single-click" value="Registrar Frases" />
                            </form>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                        @if (Session::has('status'))
                            @if (Session::get('status') == 'success')
                                <br /><div class="alert alert-success"><span class="glyphicon glyphicon-check"></span> Se han cargado las frases exitosamente</div>
                            @elseif (Session::get('status') == 'fail')
                                <br /><div class="alert alert-danger"><span class="glyphicon glyphicon-alert"></span> No se pudo cargar las frases</div>
                            @endif
                        @endif
                        </div>
                    </div>
                </div>
            </div>

        <div class="row">
            <div class="col-md-12">
                @if (Session::has('status'))
                    @if (Session::get('status') == 'frase_deleted')
                        <div class="alert alert-success"><span class="glyphicon glyphicon-check"></span> Se ha eliminado la frase</div>
                    @endif
                @endif

                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover table-condensed">
                        <caption>Frases Registradas</caption>
                        <thead>
                        <tr>
                            <th>Acciones</th>
                            <th>Frase</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($frases as $frase)
                            <tr>
                                <td>
                                    <a class="delete-token" href="{{ route('delete_frase', array('id' => $frase->id)) }}"><span class="glyphicon glyphicon-trash"></span> Eliminar</a>
                                </td>
                                <td>{{ $frase->frase }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>

                {!! $frases->render() !!}
            </div>
        </div>
    </section>
@stop
<!DOCTYPE html>
<html>
<head>
    <title>@yield('title')</title>
    <link href="{{ asset('/bootstrap-3.3.6-dist/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/css/estilo.css') }}" rel="stylesheet" type="text/css" />
    @yield('styles')
</head>
<body>
<header>
    <nav class="navbar navbar-inverse navbar-static-top" role="navigation">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navegacion-am">
                    <span class="sr-only">Desplegar / Ocultar Menu</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a href="{{ route('home') }}" class="navbar-brand">Autoliker</a>
            </div>
            <!-- Inicia Menu -->
            <div class="collapse navbar-collapse" id="navegacion-am">
                <ul class="nav navbar-nav">
                    <li class="{{ isset($home_active) ? 'active' : '' }}"><a href="{{ route('home') }}">Inicio</a></li>
                    <li class="{{ isset($admin_active) ? 'active' : '' }}"><a href="{{ route('admin') }}">Administrar Tokens</a></li>
                    <li class="{{ isset($frases_active) ? 'active' : '' }}"><a href="{{ route('frases') }}">Administrar Frases</a></li>
                    @if (\App\Helpers\SessionHelper::isLoggedIn())
                    <li><a href="{{ route('logout') }}">Cerrar Sesión</a></li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>
</header>
@yield('content')
<script src="{{ asset('/jquery/jquery-2.1.4.min.js') }}"></script>
<script src="{{ asset('/bootstrap-3.3.6-dist/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('/js/common.js') }}"></script>
@yield('scripts')
</body>
</html>

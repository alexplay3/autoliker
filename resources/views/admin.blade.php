@extends('base')

@section('title')
    Administración de Tokens
@stop

@section('content')
    <section class="container-fluid">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">Información de la Aplicación</h3>
            </div>
            <div class="panel-body">
                <form method="post" action="{{ route('update_app_info') }}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="app_id" class="control-label">Ingrese el Identificador de la Aplicación</label>
                                <input id="app_id" class="form-control" type="text" name="app_id" value="{{ $appInfo->app_id }}" required="required"/>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="app_secret" class="control-label">Ingrese la Clave Secreta de la Aplicación</label>
                                <input id="app_secret" class="form-control" type="text" name="app_secret" value="{{ $appInfo->app_secret }}" required="required"/>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="redirect_uri" class="control-label">Ingrese la direccion URL de la aplicación</label>
                                <input id="redirect_uri" class="form-control" type="text" name="redirect_uri" value="{{ $appInfo->redirect_uri }}" required="required"/>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <input type="submit" name="update_app_info" class="btn btn-primary single-click" value="Actualizar Aplicación" />
                        </div>
                        <div class="col-md-12">
                            @if (Session::has('status'))
                                @if (Session::get('status') == 'app_updated')
                                    <br />
                                    <div class="alert alert-success"><span class="glyphicon glyphicon-check"></span> Se ha actualizado la aplicación exitosamente.</div>
                                @endif
                            @endif
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">Tokens de Acceso</h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="alert alert-info"><span class="glyphicon glyphicon-info-sign"></span> Inicie sesión con la cuenta de Facebook deseada y posteriormente capture el Token.</div>
                                <a href="{{ \App\Helpers\GraphApiHelper::captureAccessTokenLinkHTCSense() }}" class="btn btn-primary single-click" target="_blank">Capturar Token de Acceso</a>
                                <br />
                                <br />
                            </div>
                            <div class="col-md-6">
                                <form method="post">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                    <div class="form-group">
                                        <label for="access_token" class="control-label">Ingrese un Token de Acceso</label>
                                        <input id="access_token" class="form-control" type="text" name="access_token" required="required"/>
                                    </div>
                                    <input type="submit" name="register_token" class="btn btn-primary single-click" value="Registrar Token Individual" />
                                </form>
                            </div>
                            <div class="col-md-6">
                                <form method="post" enctype="multipart/form-data">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                    <div class="form-group">
                                        <label for="access_token_file" class="control-label">Cargue los Tokens de Acceso desde un Archivo</label>
                                        <input id="access_token_file" class="form-control" type="file" name="access_token_file" required="required"/>
                                    </div>
                                    <input type="submit" name="register_token_multiple" class="btn btn-primary single-click" value="Registrar Tokens" />
                                </form>
                            </div>
                        </div>
                        <br />
                        @if (Session::has('status'))
                            @if (Session::get('status') == 'success')
                                <div class="alert alert-success"><span class="glyphicon glyphicon-check"></span> Se han cargado los tokens exitosamente</div>
                            @elseif (Session::get('status') == 'duplicated')
                                <div class="alert alert-danger"><span class="glyphicon glyphicon-alert"></span> No se pudo cargar los tokens. Ya existe un Token cuyo ID usuario concuerda.</div>
                            @elseif (Session::get('status') == 'fail')
                                <div class="alert alert-danger"><span class="glyphicon glyphicon-alert"></span> No se pudo cargar los tokens. Posiblemente tenga un formato incorrecto o haya expirado.</div>
                            @endif
                        @endif
                    </div>
                </div>
            </div>
        </div>

<div class="row">
        <form method="get">
            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
            <div class="col-md-2">
                <div class="form-group">
                    <input class="form-control" type="text" name="search" placeholder="Buscar por ID o Nombre Usuario" />
                </div>
            </div>
            <div class="col-md-10">
                <input type="submit" class="btn btn-primary single-click" value="Buscar" />
            </div>
        </form>
</div>

        <div class="row">
            <div class="col-md-12">
                @if (Session::has('status'))
                    @if (Session::get('status') == 'token_deleted')
                        <div class="alert alert-success"><span class="glyphicon glyphicon-check"></span> Se ha eliminado el token</div>
                    @endif
                @endif

                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover table-condensed">
                        <caption>Tokens de Acceso Registrados</caption>
                        <thead>
                            <tr>
                                <th>Acciones</th>
                                <th>Fecha de Registro</th>
                                <th>ID Usuario</th>
                                <th>Nombre Usuario</th>
                                <!--<th>Token de Acceso</th>-->
                            </tr>
                        </thead>
                        <tbody>
                        @foreach ($tokens as $token)
                            <tr>
                                <td>
                                    <a class="delete-token" href="{{ route('delete_token', array('id' => $token->id)) }}"><span class="glyphicon glyphicon-trash"></span> Eliminar</a>
                                    <!-- <a href="#"><span class="glyphicon glyphicon-refresh"></span> Regenerar</a> -->
                                </td>
                                <td>{{ $token->created_at->format('d/m/Y') }}</td>
                                <td>{{ $token->user_id }}</td>
                                <td>{{ $token->name }}</td>
                                {{--<td>{{ $token->access_token }}</td>--}}
                            </tr>
                            {{--
                            <tr class="danger">
                                <td>asd</td>
                                <td>01/01/2015</td>
                                <td>
                                    <a href="#"><span class="glyphicon glyphicon-trash"></span> Eliminar</a>
                                    <a href="#"><span class="glyphicon glyphicon-refresh"></span> Regenerar</a>
                                </td>
                            </tr>
                            <tr>
                                <td>asd</td>
                                <td>01/01/2015</td>
                                <td>
                                    <a href="#"><span class="glyphicon glyphicon-trash"></span> Eliminar</a>
                                    <a href="#"><span class="glyphicon glyphicon-refresh"></span> Regenerar</a>
                                </td>
                            </tr>
                            <tr>
                                <td>asd</td>
                                <td>01/01/2015</td>
                                <td>
                                    <a href="#"><span class="glyphicon glyphicon-trash"></span> Eliminar</a>
                                    <a href="#"><span class="glyphicon glyphicon-refresh"></span> Regenerar</a>
                                </td>
                            </tr>
                            --}}
                        @endforeach
                        </tbody>
                    </table>
                </div>

                {!! $tokens->render() !!}
            </div>
        </div>
    </section>
@stop

@section('scripts')
    <script>
        // Cuando el redirect_uri del Facebook Dialog redirije, debemos sustituir el # y recargar la pagina para que el
        // controlador tome el querystring correctamente
        $(document).ready(function() {
           if (window.location.href.indexOf('#') >= 0) {
               window.location.href = window.location.href.replace('#', '');
           }
        });
    </script>
@stop
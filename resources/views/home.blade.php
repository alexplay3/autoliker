@extends('base')

@section('title')
Autoliker
@stop

@section('content')
<section class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            @if (Session::has('status'))
                @if (Session::get('status') == 'like_success')
                    <div class="alert alert-success"><span class="glyphicon glyphicon-check"></span> Se han posteado todos los likes exitosamente</div>
                @elseif (Session::get('status') == 'dislike_success')
                    <div class="alert alert-success"><span class="glyphicon glyphicon-check"></span> Se han posteado todos los unlikes exitosamente</div>
                @elseif (Session::get('status') == 'like_with_errors')
                    <div class="alert alert-warning"><span class="glyphicon glyphicon-warning-sign"></span> ID de publicación inválido o ciertos tokens de acceso son inválidos o han expirado:
                        <ul>
                        @foreach (Session::get('like_errors') as $error)
                                <li>{{ $error }}</li>
                        @endforeach
                        </ul>
                    </div>
                @elseif (Session::get('status') == 'comment_success')
                    <div class="alert alert-success"><span class="glyphicon glyphicon-check"></span> Se han posteado todos los comentarios exitosamente</div>
                @elseif (Session::get('status') == 'comment_with_errors')
                    <div class="alert alert-warning"><span class="glyphicon glyphicon-warning-sign"></span> ID de publicación inválido, frase seleccionada vacía o ciertos tokens de acceso son inválidos o han expirado:
                        <ul>
                            @foreach (Session::get('comment_errors') as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            @endif
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Publicaciones</h3>
                </div>
                <div class="panel-body">
                    <form method="post" action="{{ route('post_like_custom') }}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}" />

                        <div class="form-group">
                            <div class="col-md-3">
                                <label for="post_id" class="control-label">ID Publicación</label>
                                <input type="text" id="post_id" name="post_id" value="" class="form-control" required="required"/>
                            </div>

                            <div class="col-md-3">
                                <label for="likes_amount" class="control-label">Posts</label>
                                <input id="likes_amount" name="likes_amount" class="form-control" type="number" min="1" max="{{ $maxTokens }}" value="1" required="required"/>
                            </div>
                            <div class="col-md-3">
                                <label class="control-label">Máximo</label>
                                <input type="text" class="form-control" value="{{ $maxTokens }}" disabled="disabled"/>
                            </div>

                            <div class="col-md-3">
                                <label for="post_delay" class="control-label">Retardo entre Posts (segs.)</label>
                                <input id="post_delay" name="post_delay" class="form-control" type="number" min="0" max="10" value="0" required="required"/>
                            </div>
                        </div>
                        <div class="col-md-12" style="margin-top: 2%;">
                            <input type="submit" name="like" class="btn btn-primary" value="Like" />
                            <input type="submit" name="unlike" class="btn btn-primary" value="Unlike" />
                            <input type="submit" name="comment" class="btn btn-primary" value="Comentar" />
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            @if (Session::has('status'))
                @if (Session::get('status') == 'share_success')
                    <div class="alert alert-success"><span class="glyphicon glyphicon-check"></span> Se ha compartido el link exitosamente. Usuarios que compartieron:
                        <ul>
                            @foreach (Session::get('share_users') as $user)
                                <li>{{ $user }}</li>
                            @endforeach
                        </ul>
                    </div>
                @elseif (Session::get('status') == 'share_with_errors')
                    <div class="alert alert-warning"><span class="glyphicon glyphicon-warning-sign"></span> ID de publicación inválido o ciertos tokens de acceso son inválidos o han expirado:
                        <ul>
                            @foreach (Session::get('share_errors') as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            @endif
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Compartir Enlaces</h3>
                </div>
                <div class="panel-body">
                    <form method="post" action="{{ route('share_link') }}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}" />

                        <div class="form-group">
                            <div class="col-md-6">
                                <label for="link" class="control-label">Link</label>
                                <input type="text" id="link" name="link" value="" class="form-control" required="required"/>
                            </div>
                            <div class="col-md-6">
                                <label for="message" class="control-label">Mensaje</label>
                                <input type="text" id="message" name="message" value="" class="form-control" required="required"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-4">
                                <label for="likes_amount" class="control-label">Posts</label>
                                <input id="likes_amount" name="likes_amount" class="form-control" type="number" min="1" max="{{ $maxTokens }}" value="1" required="required"/>
                            </div>
                            <div class="col-md-4">
                                <label class="control-label">Máximo</label>
                                <input type="text" class="form-control" value="{{ $maxTokens }}" disabled="disabled"/>
                            </div>
                            <div class="col-md-4">
                                <label for="post_delay" class="control-label">Retardo entre Posts (segs.)</label>
                                <input id="post_delay" name="post_delay" class="form-control" type="number" min="0" max="10" value="0" required="required"/>
                            </div>
                        </div>
                        <div class="col-md-12" style="margin-top: 2%;">
                            <input type="submit" name="share_group" class="btn btn-primary" value="Compartir en Grupos" />
                            <input type="submit" name="share_feed" class="btn btn-primary" value="Compartir en Muro" />
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    {{--
    <hr />
    <div class="row">
        <div class="col-md-12">
            @if (\App\Helpers\SessionHelper::isLoggedIn())
                <h3>Ha iniciado sesión como: <small>{{ \App\Helpers\SessionHelper::getUser()->name }}</small></h3>
            @else
                <form method="post">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                    <div class="form-group">
                        <label for="access_token" class="control-label">Ingrese Token de Acceso de la Fan Page</label>
                        <input id="access_token" class="form-control" type="text" name="access_token" required="required"/>
                    </div>
                    <input type="submit" name="iniciar_sesion" class="btn btn-primary single-click" value="Iniciar Sesión" />
                </form>
                @if (Session::has('status'))
                    @if (Session::get('status') == 'login_fail')
                        <br />
                        <div class="alert alert-danger"><span class="glyphicon glyphicon-alert"></span> El Token de Acceso ha Expirado</div>
                    @endif
                @endif
            @endif
        </div>
    </div>
    <hr />

    <div class="row">
        <div class="col-md-12">
            <h3>Publicaciones de la Fan Page</h3>
        </div>
    </div>
    @if ($feed != null)
        @if ($feed['status'] == 'success')
            @foreach ($feed['data'] as $pub)
                <div class="row publicacion">
                    <div class="col-md-2">
                        @if ($pub->type == 'photo')
                            <img class="img-responsive img-thumbnail" src="{{ $pub->picture }}" alt="" />
                        @else
                            <img class="img-responsive img-thumbnail" src="{{ asset('/images/publicacion_placeholder.png') }}" alt="" />
                        @endif
                    </div>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-md-6">
                                <h4>{{ $pub->from->name }}</h4>
                            </div>
                            <!--<div class="col-md-4">
                                <h4>Likes: <small>54</small></h4>
                            </div>-->
                            <div class="col-md-6">
                                <h4>ID Publicación: <small>{{ $pub->id }}</small></h4>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">

                            </div>
                        </div>
                        <hr />
                        <div class="row">
                            <div class="col-md-12">
                                <div class="publicacion-content">
                                    @if ($pub->type == 'status')
                                        <p class="text-justify">{{ $pub->message }}</p>
                                    @elseif ($pub->type == 'photo')
                                        <p class="text-justify">{{ $pub->story }}</p>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <form method="post" action="{{ route('post_like', array('post_id' => $pub->id)) }}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                            <input type="hidden" name="publication_id" value="" />
                            <div class="form-group">
                                <div class="col-md-6">
                                    <label for="likes_amount" class="control-label">Posts</label>
                                    <input id="likes_amount" name="likes_amount" class="form-control" type="number" min="1" max="{{ $maxTokens }}" value="1" required="required"/>
                                </div>
                                <div class="col-md-6">
                                    <label class="control-label">Máximo</label>
                                    <input type="text" class="form-control" value="{{ $maxTokens }}" disabled="disabled"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label for="post_delay" class="control-label">Retardo entre Posts (segs.)</label>
                                    <input id="post_delay" name="post_delay" class="form-control" type="number" min="0" max="10" value="1" required="required"/>
                                </div>
                            </div>
                            <div class="col-md-12" style="margin-top: 2%;">
                                <input type="submit" name="like" class="btn btn-primary" value="Like" />
                                <!--<input type="submit" name="comment" class="btn btn-primary" value="Comentar" />-->
                            </div>
                        </form>
                    </div>
                </div>
            @endforeach
        @else
            <div class="alert alert-info"><span class="glyphicon glyphicon-info-sign"></span> No existen publicaciones para mostrar.</div>
        @endif
    @else
        <div class="alert alert-info"><span class="glyphicon glyphicon-info-sign"></span> Usted no ha iniciado sesión.</div>
    @endif
    --}}
</section>
@stop


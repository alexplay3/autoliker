/**
 * Created by alexplay on 05/12/15.
 */

var SingleClick = function() {
    this.init = function() {
        $('form').on('submit', function() {
           $('form input[type="submit"]').css('visibility', 'hidden');
        });
    };
};

var ConfirmTokenDelete = function() {
    this.init = function() {
        $('.delete-token').on('click', function(ev) {
           if (!confirm('¿Esta seguro que desea eliminar este registro?')) {
               ev.preventDefault();
           }
        });
    };
};

$(document).ready(function() {
    new SingleClick().init();
    new ConfirmTokenDelete().init();
});
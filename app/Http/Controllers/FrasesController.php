<?php

namespace App\Http\Controllers;

use App\Frases;
use App\Helpers\FileHelper;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class FrasesController extends Controller
{
    public function index() {
        return view('frases', array(
            'frases_active' => true,
            'frases' => Frases::paginate(),
        ));
    }

    private function storeFrase($content) {
        $frase = new Frases();
        $frase->frase = $content;
        $frase->save();

        return array('status' => 'success');
    }

    public function store() {
        if (\Input::has('frase')) {
            $response = $this->storeFrase(\Input::get('frase'));

            if ($response['status'] == 'success') {
                return \Redirect::route('frases')->with('status', 'success');
            }

            return \Redirect::route('frases')->with('status', 'fail');
        } elseif (\Input::hasFile('frases_file')) {
            $upload = FileHelper::uploadFile(\Input::file('frases_file'));

            if ($upload != null) {
                $content = file_get_contents(FileHelper::getAccessTokensFileFullPath());
                $lines = explode("\n", $content);

                foreach ($lines as $line) {
                    if (empty(trim($line))) {
                        continue;
                    }

                    $this->storeFrase($line);
                }

                return \Redirect::route('frases')->with('status', 'success');
            }
        }

        return \Redirect::route('frases')->with('status', 'fail');
    }

    public function delete($id) {
        Frases::find($id)->delete();

        return \Redirect::route('frases')->with('status', 'frase_deleted');
    }
}

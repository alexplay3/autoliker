<?php

namespace App\Http\Controllers;

use App\AccessToken;
use App\Frases;
use App\Helpers\SessionHelper;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Helpers\GraphApiHelper;

class HomeController extends Controller
{
    const SESSION_NAME = 'FANPAGE_ACCESS_TOKEN';

    public function index() {
        $feed = null;
        $maxTokens = AccessToken::count();

        if (SessionHelper::isLoggedIn()) {
            $feed = GraphApiHelper::getFeed(SessionHelper::getAccessToken());
        }

        return view('home', array(
            'home_active' => true,
            'feed' => $feed,
            'maxTokens' => $maxTokens
        ));
    }

    public function login() {
        $accessToken = \Input::get('access_token');
        $response = GraphApiHelper::isValidAccessToken($accessToken);

        if ($response['status'] == 'success') {
            \Session::put(self::SESSION_NAME, array(
                'access_token' => $accessToken,
                'user' => $response['data']
            ));

            return \Redirect::route('home');
        } else {
            return \Redirect::route('home')->with('status', 'login_fail');
        }
    }

    public function logout() {
        \Session::forget(self::SESSION_NAME);

        return \Redirect::route('home');
    }

    public function postLike($post_id) {
        $limit = \Input::get('likes_amount');
        $delay = \Input::get('post_delay');

        $accessTokens = AccessToken::getRandom($limit);
        $errors = array();

        set_time_limit(0); // Para hacer todos los likes y que no se agote el tiempo de espera
        foreach ($accessTokens as $token) {
            $response = GraphApiHelper::postLike($post_id, $token->access_token);

            if ($response['status'] == 'fail') {
                array_push($errors, $token->name . ' (ID Usuario: ' . $token->user_id . ')');
            }

            sleep($delay); // TODO: Ver si en lugar de esto, se puede hacer con setTimeout en AJAX
        }

        if (count($errors) > 0) {
            return \Redirect::route('home')->with('status', 'like_with_errors')->with('like_errors', $errors);
        } else {
            return \Redirect::route('home')->with('status', 'like_success');
        }
    }

    public function postLikeCustom() {
        if (\Input::has('like')) {
            return $this->likeCustom();
        } elseif (\Input::has('unlike')) {
            return $this->unlikeCustom();
        } elseif (\Input::has('comment')) {
            return $this->postComment();
        } else {
            return \Redirect::route('home');
        }
    }

    private function likeCustom() {
        $limit = \Input::get('likes_amount');
        $delay = \Input::get('post_delay');
        $post_id = \Input::get('post_id');

        $accessTokens = AccessToken::getRandom($limit);
        $errors = array();

        set_time_limit(0); // Para hacer todos los likes y que no se agote el tiempo de espera
        foreach ($accessTokens as $token) {
            $response = GraphApiHelper::postLike($post_id, $token->access_token);

            if ($response['status'] == 'fail') {
                array_push($errors, $token->name . ' (ID Usuario: ' . $token->user_id . ')');
            }

            sleep($delay); // TODO: Ver si en lugar de esto, se puede hacer con setTimeout en AJAX
        }

        if (count($errors) > 0) {
            return \Redirect::route('home')->with('status', 'like_with_errors')->with('like_errors', $errors);
        } else {
            return \Redirect::route('home')->with('status', 'like_success');
        }
    }

    private function unlikeCustom() {
        $post_id = \Input::get('post_id');

        $accessTokens = AccessToken::all();
        $errors = array();

        set_time_limit(0); // Para hacer todos los unlikes y que no se agote el tiempo de espera
        foreach ($accessTokens as $token) {
            $response = GraphApiHelper::deleteLike($post_id, $token->access_token);

            if ($response['status'] == 'fail') {
                array_push($errors, $token->name . ' (ID Usuario: ' . $token->user_id . ')');
            }
        }

        if (count($errors) > 0) {
            return \Redirect::route('home')->with('status', 'like_with_errors')->with('like_errors', $errors);
        } else {
            return \Redirect::route('home')->with('status', 'dislike_success');
        }
    }

    public function shareLink() {
        $limit = \Input::get('likes_amount');
        $delay = \Input::get('post_delay');
        $post_id = \Input::get('post_id');
        $link = \Input::get('link');
        $message = \Input::get('message');

        $accessTokens = AccessToken::getRandom($limit);
        $errors = array();
        $usuarios = array(); // Los usuarios que se utilizaron para compartir los enlaces

        set_time_limit(0); // Para hacer todos los share y que no se agote el tiempo de espera
        foreach ($accessTokens as $token) {
            if (\Input::has('share_group')) {
                $data = GraphApiHelper::getUserGroups($token->user_id, $token->access_token);

                if ($data['status'] == 'success') {
                    // Para obtener todos los grupos de cada usuario, no recomendable, ya que bloquean la cuenta
                    /*foreach ($data['groups'] as $group) {
                        $tags = array();
                        $dataGroup = GraphApiHelper::getGroupMembers($group->id, $token->access_token);

                        if ($dataGroup['status'] == 'success') {
                            foreach ($dataGroup['members'] as $member) {
                                array_push($tags, $member->id);
                            }
                        }

                        $response = GraphApiHelper::shareLink($group->id, $token->access_token, $link, $message, $tags);

                        if ($response['status'] == 'fail') {
                            array_push($errors, $token->name . ' (ID Usuario: ' . $token->user_id . ', ID Grupo: ' . $group->id . ')');
                        } else {
                            array_push($usuarios, $token->name . ' (ID Usuario: ' . $token->user_id . ')');
                        }
                    }*/


                    // Extrae un grupo al azar, para evitar bloqueos en cuenta
                    // No podemos hacerlo en todos los grupos de todos los usuarios, ya que nos bloquearán
                    $tags = array();
                    $group = $data['groups'][array_rand($data['groups'], 1)];
                    $dataGroup = GraphApiHelper::getGroupMembers($group->id, $token->access_token);

                    if ($dataGroup['status'] == 'success') {
                        foreach ($dataGroup['members'] as $member) {
                            array_push($tags, $member->id);
                        }
                    }

                    $response = GraphApiHelper::shareLink($group->id, $token->access_token, $link, $message, $tags);

                    if ($response['status'] == 'fail') {
                        array_push($errors, $token->name . ' (ID Usuario: ' . $token->user_id . ', ID Grupo: ' . $group->id . ')');
                    } else {
                        array_push($usuarios, $token->name . ' (ID Usuario: ' . $token->user_id . ')');
                    }
                } else {
                    continue;
                }
            } elseif (\Input::has('share_feed')) {
                $tags = array();
                $data = GraphApiHelper::getFriends($token->user_id, $token->access_token);

                if ($data['status'] == 'success') {
                    foreach ($data['friends'] as $friend) {
                        array_push($tags, $friend->id);
                    }
                }

                $response = GraphApiHelper::shareLink($token->user_id, $token->access_token, $link, $message, $tags);

                if ($response['status'] == 'fail') {
                    array_push($errors, $token->name . ' (ID Usuario: ' . $token->user_id . ')');
                } else {
                    array_push($usuarios, $token->name . ' (ID Usuario: ' . $token->user_id . ')');
                }
            } else {
                break;
            }

            sleep($delay); // TODO: Ver si en lugar de esto, se puede hacer con setTimeout en AJAX
        }

        if (count($errors) > 0) {
            return \Redirect::route('home')->with('status', 'share_with_errors')->with('share_errors', $errors);
        } else {
            return \Redirect::route('home')->with('status', 'share_success')->with('share_users', $usuarios);
        }
    }

    public function postComment() {
        $limit = \Input::get('likes_amount');
        $delay = \Input::get('post_delay');
        $post_id = \Input::get('post_id');

        $accessTokens = AccessToken::getRandom($limit);

        $errors = array();

        set_time_limit(0); // Para hacer todos los comment y que no se agote el tiempo de espera
        foreach ($accessTokens as $token) {
            $response = GraphApiHelper::postComment($post_id, $token->access_token, Frases::getRandom());

            if ($response['status'] == 'fail') {
                array_push($errors, $token->name . ' (ID Usuario: ' . $token->user_id . ')');
            }

            sleep($delay); // TODO: Ver si en lugar de esto, se puede hacer con setTimeout en AJAX
        }

        if (count($errors) > 0) {
            return \Redirect::route('home')->with('status', 'comment_with_errors')->with('comment_errors', $errors);
        } else {
            return \Redirect::route('home')->with('status', 'comment_success');
        }
    }
}

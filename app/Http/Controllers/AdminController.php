<?php

namespace App\Http\Controllers;

use App\AccessToken;
use App\AppInfo;
use App\Helpers\FileHelper;
use App\Helpers\GraphApiHelper;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class AdminController extends Controller
{
    public function index() {
        $appInfo = AppInfo::find(1);

        // Cuando el redirect_uri del Facebook Dialog redirije, cae aqui para capturar y guardar el token automaticamente
        if (\Input::has('access_token')) {
            $response = $this->storeExtendedToken(\Input::get('access_token'));
            return \Redirect::route('admin')->with('status', $response['status']);
        }

        if (\Input::has('search')) {
            $search = \Input::get('search');

            $tokens = AccessToken::where('user_id', '=', $search)->orWhere('name', 'like', "%{$search}%")->paginate();
        } else {
            $tokens = AccessToken::paginate();
        }

        return view('admin', array(
            'admin_active' => true,
            'tokens' => $tokens,
            'appInfo' => $appInfo
        ));
    }

    private function storeExtendedToken($token) {
        $appInfo = AppInfo::find(1);
        $extend = GraphApiHelper::extendAccessToken($token, $appInfo->app_id, $appInfo->app_secret);

        if ($extend['status'] == 'success') {
            $response = $this->storeToken($extend['access_token']);

            return array('status' => $response['status']);
        } else {
            return array('status' => $extend['status']);
        }
    }

    private function storeToken($token) {
        $response = GraphApiHelper::isValidAccessToken($token);

        if ($response['status'] == 'success') {
            if (AccessToken::where('user_id', '=', $response['data']->id)->count() > 0) {
                return array('status' => 'duplicated');
            }

            $accessToken = new AccessToken();

            $accessToken->access_token = $token;
            $accessToken->user_id = $response['data']->id;
            $accessToken->name = $response['data']->name;

            $accessToken->save();

            return array('status' => 'success');
        }

        return array('status' => 'fail');
    }

    public function store() {
        $appInfo = AppInfo::find(1);

        if (\Input::has('access_token')) {
            // Si no hay AppSecret, guardar el token normal sin extenderlo
            if ($appInfo->app_secret == 'NO_DISPONIBLE') {
                $response = $this->storeToken(\Input::get('access_token'));
            } else {
                $response = $this->storeExtendedToken(\Input::get('access_token'));
            }

            if ($response['status'] == 'success') {
                return \Redirect::route('admin')->with('status', 'success');
            } else if ($response['status'] == 'duplicated') {
                return \Redirect::route('admin')->with('status', 'duplicated');
            }

            return \Redirect::route('admin')->with('status', 'fail');
        } elseif (\Input::hasFile('access_token_file')) {
            $upload = FileHelper::uploadFile(\Input::file('access_token_file'));

            // TODO: Mostrar cuales tokens no se cargaron (array de errores)
            if ($upload != null) {
                $content = file_get_contents(FileHelper::getAccessTokensFileFullPath());
                $lines = explode("\n", $content);

                foreach ($lines as $line) {
                    if (empty(trim($line))) {
                        continue;
                    }

                    $this->storeToken($line);
                }

                return \Redirect::route('admin')->with('status', 'success');
            }
        }

        return \Redirect::route('admin')->with('status', 'fail');
    }

    public function delete($id) {
        AccessToken::find($id)->delete();

        return \Redirect::route('admin')->with('status', 'token_deleted');
    }

    public function updateApp() {
        $appInfo = AppInfo::find(1);

        $appInfo->app_id = \Input::get('app_id');
        $appInfo->app_secret = \Input::get('app_secret');
        $appInfo->redirect_uri = \Input::get('redirect_uri');

        $appInfo->update();

        return \Redirect::route('admin')->with('status', 'app_updated');
    }
}

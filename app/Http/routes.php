<?php

Route::get('/', array(
    'as' => 'home',
    'uses' => 'HomeController@index')
);
Route::post('/', array(
    'as' => 'login',
    'uses' => 'HomeController@login')
);
Route::get('/logout', array(
    'as' => 'logout',
    'uses' => 'HomeController@logout')
);
Route::get('/admin', array(
    'as' => 'admin',
    'uses' => 'AdminController@index'
));
Route::get('/frases', array(
    'as' => 'frases',
    'uses' => 'FrasesController@index'
));
Route::post('/admin', array(
    'as' => 'store_token',
    'uses' => 'AdminController@store'
));
Route::get('/admin/delete-token/{id}', array(
    'as' => 'delete_token',
    'uses' => 'AdminController@delete'
));
Route::post('/post-like/{post_id}', array(
    'as' => 'post_like',
    'uses' => 'HomeController@postLike'
));
Route::post('/post-like', array(
    'as' => 'post_like_custom',
    'uses' => 'HomeController@postLikeCustom'
));
Route::post('/admin/update-app-info', array(
    'as' => 'update_app_info',
    'uses' => 'AdminController@updateApp'
));
Route::post('/share-link', array(
    'as' => 'share_link',
    'uses' => 'HomeController@shareLink'
));
Route::post('/post-comment', array(
    'as' => 'post_comment',
    'uses' => 'HomeController@postComment'
));

Route::post('/frases', array(
    'as' => 'store_frase',
    'uses' => 'FrasesController@store'
));
Route::get('/frases/delete-frase/{id}', array(
    'as' => 'delete_frase',
    'uses' => 'FrasesController@delete'
));
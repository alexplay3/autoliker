<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AccessToken extends Model
{
    protected $table = 'access_tokens';
    protected $fillable = ['access_token', 'user_id', 'name'];

    public static function getRandom($limit) {
        $count = self::count();

        if ($limit > $count) {
            $limit = $count;
        }

        $result = self::all()->random($limit);
        return $limit > 1 ? $result : array($result);

        /*$tokens = self::all();
        self::shuffleTokens($tokens);

        return $tokens->slice(0, $limit);*/
    }

    /*private static function shuffleTokens($tokens) {
        $size = count($tokens);

        for ($i = 0; $i < $size; $i++) {
            $r1 = rand(0, $size - 1);
            $r2 = rand(0, $size - 1);

            $tmp = $tokens[$r1];
            $tokens[$r1] = $tokens[$r2];
            $tokens[$r2] = $tmp;
        }
    }*/
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AppInfo extends Model
{
    protected $table = 'app_info';
    protected $fillable = ['app_id', 'app_secret', 'redirect_uri'];
}

<?php
/**
 * Created by PhpStorm.
 * User: alexplay
 * Date: 05/12/15
 * Time: 01:06 PM
 */

namespace App\Helpers;


use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class FileHelper
{
    const ACCESS_TOKENS_PATH = '/upload/';
    const ACCESS_TOKENS_FILE = 'access_tokens.txt';

    public static function uploadFile(UploadedFile $file) {
        $destinationPath = self::ACCESS_TOKENS_PATH;
        $filename = self::ACCESS_TOKENS_FILE;

        return $file->move(public_path() . $destinationPath, $filename);
    }

    public static function getAccessTokensFileFullPath() {
        return public_path() . self::ACCESS_TOKENS_PATH . self::ACCESS_TOKENS_FILE;
    }
}
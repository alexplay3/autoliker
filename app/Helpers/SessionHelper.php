<?php
/**
 * Created by PhpStorm.
 * User: alexplay
 * Date: 05/12/15
 * Time: 01:17 AM
 */

namespace App\Helpers;


use App\Http\Controllers\HomeController;

class SessionHelper
{
    public static function isLoggedIn() {
        return \Session::has(HomeController::SESSION_NAME);
    }

    public static function getAccessToken() {
        if (!self::isLoggedIn()) {
            return null;
        }

        return \Session::get(HomeController::SESSION_NAME)['access_token'];
    }

    public static function getUser() {
        if (!self::isLoggedIn()) {
            return null;
        }

        return \Session::get(HomeController::SESSION_NAME)['user'];
    }
}
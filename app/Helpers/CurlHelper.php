<?php
/**
 * Created by PhpStorm.
 * User: alexplay
 * Date: 05/12/15
 * Time: 01:25 AM
 */

namespace app\Helpers;


class CurlHelper
{
    public static function request($url) {
        $ch = curl_init();
        curl_setopt_array($ch, array(
                CURLOPT_CONNECTTIMEOUT => 5,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_TIMEOUT_MS => 0,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_URL => $url,
                CURLOPT_SSL_VERIFYPEER => 0
            )
        );
        $result = curl_exec($ch);
        curl_close($ch);

        return json_decode($result);
    }
}
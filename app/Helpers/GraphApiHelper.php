<?php
/**
 * Created by PhpStorm.
 * User: alexplay
 * Date: 05/12/15
 * Time: 03:35 PM
 */

namespace App\Helpers;


use App\AppInfo;

class GraphApiHelper
{
    const API_URL = 'https://graph.fb.me';

    private static function request($url) {
        return CurlHelper::request(self::API_URL . $url);
    }

    public static function captureAccessTokenLink() {
        $appInfo = AppInfo::find(1);
        $permissions = 'publish_actions,publish_pages,user_about_me,user_likes,user_posts,user_status';

        return  'https://www.facebook.com/dialog/oauth?'.
                'scope='.$permissions.
                '&redirect_uri='.$appInfo->redirect_uri.
                '&response_type=token&client_id='.$appInfo->app_id;
    }

    public static function captureAccessTokenLinkHTCSense() {
        $appInfo = AppInfo::find(1);
        $permissions = 'email,publish_actions,user_about_me,user_actions.music,user_actions.news,user_actions.video,user_activities,user_birthday,user_education_history,user_events,user_games_activity,user_groups,user_hometown,user_interests,user_likes,user_location,user_notes,user_photos,user_questions,user_relationship_details,user_relationships,user_religion_politics,user_status,user_subscriptions,user_videos,user_website,user_work_history,friends_about_me,friends_actions.music,friends_actions.news,friends_actions.video,friends_activities,friends_birthday,friends_education_history,friends_events,friends_games_activity,friends_groups,friends_hometown,friends_interests,friends_likes,friends_location,friends_notes,friends_photos,friends_questions,friends_relationship_details,friends_relationships,friends_religion_politics,friends_status,friends_subscriptions,friends_videos,friends_website,friends_work_history,ads_management,create_event,create_note,export_stream,friends_online_presence,manage_friendlists,manage_notifications,manage_pages,offline_access,photo_upload,publish_checkins,publish_stream,read_friendlists,read_insights,read_mailbox,read_page_mailboxes,read_requests,read_stream,rsvp_event,share_item,sms,status_update,user_online_presence,video_upload,xmpp_login';

        return  'https://www.facebook.com/v1.0/dialog/oauth?'.
        'scope='.$permissions.
        '&redirect_uri='.$appInfo->redirect_uri.
        '&response_type=token&client_id='.$appInfo->app_id;
    }

    public static function isValidAccessToken($accessToken) {
        $response = self::request('/me?access_token=' . $accessToken);

        if (isset($response->error) && $response->error->type == 'OAuthException') {
            return array('status' => 'fail');
        } else {
            return array(
                'status' => 'success',
                'data' => $response
            );
        }
    }

    public static function extendAccessToken($accessToken, $appId, $appSecret) {
        //$response = self::request('/oauth/access_token?grant_type=fb_exchange_token&client_id='.$appId.'&client_secret='.$appSecret.'&fb_exchange_token='.$accessToken);

        try {
            $response = file_get_contents(self::API_URL . '/oauth/access_token?grant_type=fb_exchange_token&client_id=' . $appId . '&client_secret=' . $appSecret . '&fb_exchange_token=' . $accessToken);

            return array(
                'status' => 'success',
                'access_token' => explode('&', explode('=', $response)[1])[0] // El formato viene asi: access_token=XXXX&expires=XXXX
            );
        } catch (\ErrorException $ex) {
            return array('status' => 'fail');
        }
    }

    public static function getFeed($accessToken) {
        $response = self::request('/me/feed?access_token=' . $accessToken);

        if (count($response->data) >= 1) {
            return array(
                'status' => 'success',
                'data' => $response->data
            );
        } else {
            return array('status' => 'empty');
        }
    }

    public static function postLike($postId, $accessToken) {
        $response = self::request('/'.$postId.'/likes?access_token='.$accessToken.'&method=post');

        if (isset($response->success) || $response == '1') {
            return array('status' => 'success');
        } else {
            return array('status' => 'fail');
        }
    }

    public static function deleteLike($postId, $accessToken) {
        $response = self::request('/'.$postId.'/likes?access_token='.$accessToken.'&method=delete');

        if (isset($response->success) || $response == '1') {
            return array('status' => 'success');
        } else {
            return array('status' => 'fail');
        }
    }

    public static function postComment($postId, $accessToken, $comment) {
        $response = self::request('/'.$postId.'/comments?method=post&access_token='.$accessToken.'&message='.urlencode($comment));

        if (isset($response->id)) {
            return array('status' => 'success');
        } else {
            return array('status' => 'fail');
        }
    }

    /**
     * Procedimiento para compartir un link:
     *
     * En grupos de usuarios
     * 1.Obtenemos los grupos
     * 2.Por cada grupo, obtenemos sus miembros
     * 3.Etiquetamos a los miembros del grupo, y hacemos share
     *
     * En feed de cada usuario
     * 1.Obtenemos amigos
     * 2.Si hay amigos, etiquetarlos y share
     * 3.Si no hay amigos, share
     *
     * @param $objectId <p>Can be either User ID or Group ID</p>
     * @param $accessToken
     * @param $link <p>Link to share in the wall</p>
     * @param $message <p>Message to post alongside the link</p>
     * @param $tags <p>array of users id to tag on the post (when posting to wall, the users need to be friends)</p>
     * @return array
     */
    public static function shareLink($objectId, $accessToken, $link, $message, $tags = array()) {
        // Just in case, remember: "place" field could be necessary to tag users on the post
        $response = self::request('/'.$objectId.'/feed?method=post&access_token='.$accessToken.'&link='.urlencode($link).'&message='.urlencode($message).'&tags='.implode(',', $tags));

        if (isset($response->id)) {
            return array('status' => 'success');
        } else {
            return array('status' => 'fail');
        }
    }

    public static function getUserGroups($userId, $accessToken) {
        $response = self::request('/'.$userId.'/groups?access_token='.$accessToken);

        /*
         * data fields: name, privacy, version, id
         */
        if (!isset($response->data) || count($response->data) == 0) { // No pertenece a algun grupo
            return array('status' => 'empty');
        } else {
            return array(
                'status' => 'success',
                'groups' => $response->data
            );
        }
    }

    /**
     * Only gets members from a non-secret group
     *
     * @param $groupId
     * @param $accessToken
     * @return array
     */
    public static function getGroupMembers($groupId, $accessToken) {
        $response = self::request('/'.$groupId.'/members?access_token='.$accessToken);

        /*
         * data fields: name, administrator, id
         */
        if (!isset($response->data) || count($response->data) == 0) { // El grupo es secreto
            return array('status' => 'empty');
        } else {
            return array(
                'status' => 'success',
                'members' => $response->data
            );
        }
    }

    /**
     * Only gets friends who use our App
     *
     * @param $userId
     * @param $accessToken
     */
    public static function getFriends($userId, $accessToken) {
        $response = self::request('/'.$userId.'/friends?access_token='.$accessToken);

        /*
         * data fields: name, id
         */
        if (!isset($response->data) || count($response->data) == 0) { // No tiene amigos con la App instalada
            return array('status' => 'empty');
        } else {
            return array(
                'status' => 'success',
                'friends' => $response->data
            );
        }
    }
}
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Frases extends Model
{
    protected $table = 'frases';
    protected $fillable = ['frase'];

    public static function getRandom() {
        return self::all()->random()->frase;

        /*$frases = self::all();
        self::shuffleFrases($frases);

        return $frases->slice(0, 1)->get(0)->frase;*/
    }

    /*private static function shuffleFrases($frases) {
        $size = count($frases);

        for ($i = 0; $i < $size; $i++) {
            $r1 = rand(0, $size - 1);
            $r2 = rand(0, $size - 1);

            $tmp = $frases[$r1];
            $frases[$r1] = $frases[$r2];
            $frases[$r2] = $tmp;
        }
    }*/
}
